#pragma once
#include <stdlib.h>
#include <vector>
#include <iterator>
#include <algorithm>
#include "Card.h"
class Deck
{
public:
	vector<Card> deckVector;
	enum position { TOP, BOTTOM, RANDOM };

	void AddCard(Card card, position);
	void AddCard(Card card, int pos);
	void DeleteCard(Card card);
	void DeleteCard(int pos);
	int FindCardPos(Card card);
	void SwitchCardPos(Card card, Card toSwap);
	bool IsCardInDeck(Card card);
	void PopulateStandardDeck();
	void ShuffleDeck(int shuffleFreq);
	Card GetCard(Deck::position, bool isTake);



};